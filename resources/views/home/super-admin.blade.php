{{-- <div class="col-md-4">
    <!--begin::List Widget 1-->
    <div class="card card-custom gutter-b">
        <!--begin::Header-->
        <div class="card-header pt-5">
            <h3 class="card-title align-items-start flex-column">
                <span class="card-label font-weight-bolder text-dark"> @lang('pages.Users Types') </span>
                <span class="text-muted mt-3 font-weight-bold font-size-sm"> {{ $users_types->count() }} @lang('home.Types') </span>
            </h3>
            <div class="card-toolbar">
                <a href="{{ route('users-types.create') }}" class="btn btn-primary btn-sm mr-3 rounded-0">
                    <i class="flaticon2-plus"></i>@lang("common.New")
                </a>
            </div>
        </div>
        <!--end::Header-->
        <!--begin::Body-->
        <div class="card-body pt-8">
            <div class="scroll scroll-pull ps" data-scroll="true" style="height: 25em">
                @foreach ($users_types as $type)
                <!--begin::Item-->
                <div class="d-flex align-items-center mb-10">
                    <!--begin::Text-->
                    <div class="d-flex flex-column font-weight-bold">
                        <a href="{{ route('users-types.show', ['users_type' => $type->id] ) }}" class="text-dark text-hover-primary mb-1 font-size-lg">{{ $type->name }} [ {{ $type->users()->count() }} ]</a>
                        <span class="text-muted">{{ Str::limit($type->description, 100, " ... .") }}</span>
                    </div>
                    <!--end::Text-->
                </div>
                <!--end::Item-->
                @endforeach
            </div>
        </div>
        <!--end::Body-->
    </div>
    <!--end::List Widget 1-->
</div> --}}

<div class="col-xl-4 col-lg-12">
    <!--begin::List Widget 1-->
    <div class="card card-custom gutter-b">
        <!--begin::Header-->
        <div class="card-header pt-5">
            <h3 class="card-title align-items-start flex-column">
                <span class="card-label font-weight-bolder text-dark"> @lang('home.Who\'s Online') </span>
            </h3>
            <div class="card-toolbar"></div>
        </div>
        <!--end::Header-->
        <!--begin::Body-->
        <div class="card-body" id="who_is_here"></div>
        <!--end::Body-->
    </div>
    <!--end::List Widget 1-->
</div>

<div class="col-xl-8 col-lg-12">
    <!--begin::List Widget 1-->
    <div class="card card-custom card-stretch gutter-b">
        <!--begin::Header-->
        <div class="card-header pt-5">
            <h3 class="card-title align-items-start flex-column">
                <span class="card-label font-weight-bolder text-dark"> المشتركيين </span>
            </h3>
            <div class="card-toolbar"></div>
        </div>
        <!--end::Header-->
        <!--begin::Body-->
        <div class="card-body row">
            <div class="col-md-3">
                <!--begin::Stats Widget 29-->
                <div class="card card-custom bg-primary rounded-0 mb-2" style="height: 10em">
                    <!--begin::Body-->
                    <div class="card-body">
                        <i class="flaticon2-black-back-closed-envelope-shape text-white icon-2x"></i>
                        <span class="card-title font-weight-bolder text-white font-size-h2 mb-0 d-block">5,209</span>
                        <span class="font-weight-bold text-white font-size-sm">الرسائل</span>
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Stats Widget 29-->
            </div>
            <div class="col-md-3" >
                <!--begin::Stats Widget 29-->
                <div class="card card-custom bg-primary  rounded-0 mb-2" style="height: 10em">
                    <!--begin::Body-->
                    <div class="card-body">
                        <i class="fab fa-audible text-white icon-2x"></i>
                        <span class="card-title font-weight-bolder text-white font-size-h2 mb-0 d-block">16</span>
                        <span class="font-weight-bold text-white font-size-sm">الكورسات</span>
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Stats Widget 29-->
            </div>
            <div class="col-md-3">
                <!--begin::Stats Widget 29-->
                <div class="card card-custom bg-primary  rounded-0 mb-2" style="height: 10em">
                    <!--begin::Body-->
                    <div class="card-body">
                        <i class="fas fa-paperclip text-white icon-2x"></i>
                        <span class="card-title font-weight-bolder text-white font-size-h2 mb-0 d-block">24</span>
                        <span class="font-weight-bold text-white font-size-sm">المحاضرات</span>
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Stats Widget 29-->
            </div>
            <div class="col-md-3">
                <!--begin::Stats Widget 29-->
                <div class="card card-custom bg-primary  rounded-0 mb-2" style="height: 10em">
                    <!--begin::Body-->
                    <div class="card-body">
                        <i class="fas fa-clipboard-list text-white icon-2x"></i>
                        <span class="card-title font-weight-bolder text-white font-size-h2 mb-0 d-block">5,209</span>
                        <span class="font-weight-bold text-white font-size-sm">الواجبات</span>
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Stats Widget 29-->
            </div>
            <div class="col-md-3">
                <!--begin::Stats Widget 29-->
                <div class="card card-custom bg-primary rounded-0 mb-2" style="height: 10em">
                    <!--begin::Body-->
                    <div class="card-body">
                        <i class="fas fa-thumbs-up text-white icon-2x"></i>
                        <span class="card-title font-weight-bolder text-white font-size-h2 mb-0 d-block">5,209</span>
                        <span class="font-weight-bold text-white font-size-sm">التضنيفات</span>
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Stats Widget 29-->
            </div>
            <div class="col-md-3">
                <!--begin::Stats Widget 29-->
                <div class="card card-custom bg-primary rounded-0 mb-2" style="height: 10em">
                    <!--begin::Body-->
                    <div class="card-body">
                        <i class="fas fa-question text-white icon-2x"></i>
                        <span class="card-title font-weight-bolder text-white font-size-h2 mb-0 d-block">5,209</span>
                        <span class="font-weight-bold text-white font-size-sm">@lang('home.Questions')</span>
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Stats Widget 29-->
            </div>
            <div class="col-md-3">
                <!--begin::Stats Widget 29-->
                <div class="card card-custom bg-primary rounded-0 mb-2" style="height: 10em">
                    <!--begin::Body-->
                    <div class="card-body">
                        <i class="fas fa-laptop-house text-white icon-2x"></i>
                        <span class="card-title font-weight-bolder text-white font-size-h2 mb-0 d-block">5,209</span>
                        <span class="font-weight-bold text-white font-size-sm">المحاضريين</span>
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Stats Widget 29-->
            </div>
            <div class="col-md-3">
                <!--begin::Stats Widget 29-->
                <div class="card card-custom bg-primary rounded-0 mb-2" style="height: 10em">
                    <!--begin::Body-->
                    <div class="card-body">
                        <i class="fas fa-book text-white icon-2x"></i>
                        <span class="card-title font-weight-bolder text-white font-size-h2 mb-0 d-block">5,209</span>
                        <span class="font-weight-bold text-white font-size-sm">@lang('home.Preparations')</span>
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Stats Widget 29-->
            </div>

        </div>
        <!--end::Body-->
    </div>
    <!--end::List Widget 1-->
</div>
{{--<div class="col-md-4">--}}
{{--    <!--begin::List Widget 1-->--}}
{{--    <div class="card card-custom gutter-b">--}}
{{--        <!--begin::Header-->--}}
{{--        <div class="card-header pt-5">--}}
{{--            <h3 class="card-title align-items-start flex-column">--}}
{{--                <span class="card-label font-weight-bolder text-dark"> @lang('home.Pageviews/sec') </span>--}}
{{--            </h3>--}}
{{--            <div class="card-toolbar"></div>--}}
{{--        </div>--}}
{{--        <!--end::Header-->--}}
{{--        <!--begin::Body-->--}}
{{--        <div class="card-body" id="page_views"></div>--}}
{{--        <!--end::Body-->--}}
{{--    </div>--}}
{{--    <!--end::List Widget 1-->--}}
{{--</div>--}}
{{--<div class="col-md-8">--}}
{{--    <!--begin::List Widget 1-->--}}
{{--    <div class="card card-custom card-stretch gutter-b">--}}
{{--        <!--begin::Header-->--}}
{{--        <div class="card-header pt-5">--}}
{{--            <h3 class="card-title align-items-start flex-column">--}}
{{--                <span class="card-label font-weight-bolder text-dark"> @lang('home.Statistics of users in the school group') </span>--}}
{{--            </h3>--}}
{{--            <div class="card-toolbar"></div>--}}
{{--        </div>--}}
{{--        <!--end::Header-->--}}
{{--        <!--begin::Body-->--}}
{{--        <div class="card-body row">--}}
{{--            <div class="col-md-3">--}}
{{--                <!--begin::Stats Widget 29-->--}}
{{--                <div class="card card-custom bg-primary rounded-0 mb-2" style="height: 10em">--}}
{{--                    <!--begin::Body-->--}}
{{--                    <div class="card-body">--}}
{{--                        <i class="fas fa-user-graduate text-white icon-2x"></i>--}}
{{--                        <span class="card-title font-weight-bolder text-white font-size-h2 mb-0 d-block">5,209</span>--}}
{{--                        <span class="font-weight-bold text-white font-size-sm">@lang('home.Students')</span>--}}
{{--                    </div>--}}
{{--                    <!--end::Body-->--}}
{{--                </div>--}}
{{--                <!--end::Stats Widget 29-->--}}
{{--            </div>--}}
{{--            <div class="col-md-3" >--}}
{{--                <!--begin::Stats Widget 29-->--}}
{{--                <div class="card card-custom bg-primary  rounded-0 mb-2" style="height: 10em">--}}
{{--                    <!--begin::Body-->--}}
{{--                    <div class="card-body">--}}
{{--                        <i class="fas fa-user-tie text-white icon-2x"></i>--}}
{{--                        <span class="card-title font-weight-bolder text-white font-size-h2 mb-0 d-block">16</span>--}}
{{--                        <span class="font-weight-bold text-white font-size-sm">@lang('home.Teachers')</span>--}}
{{--                    </div>--}}
{{--                    <!--end::Body-->--}}
{{--                </div>--}}
{{--                <!--end::Stats Widget 29-->--}}
{{--            </div>--}}
{{--            <div class="col-md-3">--}}
{{--                <!--begin::Stats Widget 29-->--}}
{{--                <div class="card card-custom bg-primary  rounded-0 mb-2" style="height: 10em">--}}
{{--                    <!--begin::Body-->--}}
{{--                    <div class="card-body">--}}
{{--                        <i class="fas fa-user-shield text-white icon-2x"></i>--}}
{{--                        <span class="card-title font-weight-bolder text-white font-size-h2 mb-0 d-block">24</span>--}}
{{--                        <span class="font-weight-bold text-white font-size-sm">@lang('home.Managers')</span>--}}
{{--                    </div>--}}
{{--                    <!--end::Body-->--}}
{{--                </div>--}}
{{--                <!--end::Stats Widget 29-->--}}
{{--            </div>--}}
{{--            <div class="col-md-3">--}}
{{--                <!--begin::Stats Widget 29-->--}}
{{--                <div class="card card-custom bg-primary  rounded-0 mb-2" style="height: 10em">--}}
{{--                    <!--begin::Body-->--}}
{{--                    <div class="card-body">--}}
{{--                        <i class="far fa-calendar-times text-white icon-2x"></i>--}}
{{--                        <span class="card-title font-weight-bolder text-white font-size-h2 mb-0 d-block">5,209</span>--}}
{{--                        <span class="font-weight-bold text-white font-size-sm">@lang('home.Absences')</span>--}}
{{--                    </div>--}}
{{--                    <!--end::Body-->--}}
{{--                </div>--}}
{{--                <!--end::Stats Widget 29-->--}}
{{--            </div>--}}
{{--            <div class="col-md-3">--}}
{{--                <!--begin::Stats Widget 29-->--}}
{{--                <div class="card card-custom bg-primary rounded-0 mb-2" style="height: 10em">--}}
{{--                    <!--begin::Body-->--}}
{{--                    <div class="card-body">--}}
{{--                        <i class="fas fa-couch text-white icon-2x"></i>--}}
{{--                        <span class="card-title font-weight-bolder text-white font-size-h2 mb-0 d-block">5,209</span>--}}
{{--                        <span class="font-weight-bold text-white font-size-sm">@lang('home.Vacancies')</span>--}}
{{--                    </div>--}}
{{--                    <!--end::Body-->--}}
{{--                </div>--}}
{{--                <!--end::Stats Widget 29-->--}}
{{--            </div>--}}
{{--            <div class="col-md-3">--}}
{{--                <!--begin::Stats Widget 29-->--}}
{{--                <div class="card card-custom bg-primary rounded-0 mb-2" style="height: 10em">--}}
{{--                    <!--begin::Body-->--}}
{{--                    <div class="card-body">--}}
{{--                        <i class="fas fa-tasks text-white icon-2x"></i>--}}
{{--                        <span class="card-title font-weight-bolder text-white font-size-h2 mb-0 d-block">5,209</span>--}}
{{--                        <span class="font-weight-bold text-white font-size-sm">@lang('home.Teachers Workloads')</span>--}}
{{--                    </div>--}}
{{--                    <!--end::Body-->--}}
{{--                </div>--}}
{{--                <!--end::Stats Widget 29-->--}}
{{--            </div>--}}

{{--        </div>--}}
{{--        <!--end::Body-->--}}
{{--    </div>--}}
{{--    <!--end::List Widget 1-->--}}
{{--</div>--}}
{{--<div class="col-md-8">--}}
{{--    <!--begin::List Widget 1-->--}}
{{--    <div class="card card-custom gutter-b">--}}
{{--        <!--begin::Header-->--}}
{{--        <div class="card-header pt-5">--}}
{{--            <h3 class="card-title align-items-start flex-column">--}}
{{--                <span class="card-label font-weight-bolder text-dark"> @lang('home.Pageviews/sec') </span>--}}
{{--            </h3>--}}
{{--            <div class="card-toolbar"></div>--}}
{{--        </div>--}}
{{--        <!--end::Header-->--}}
{{--        <!--begin::Body-->--}}
{{--        <div class="card-body" id="apsence"></div>--}}
{{--        <!--end::Body-->--}}
{{--    </div>--}}
{{--    <!--end::List Widget 1-->--}}
{{--</div>--}}
