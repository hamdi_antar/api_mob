
@if(isset($isEmailVerified))
@if( $item->isEmailVerified())
    <i class="fa fa-check-circle text-success"></i>
@else
    <a title="تفعيل البريد الألكترونى" href="javascript:void(0);"  onclick="confirmActionWithAlert('{{'#activate_email-'.$item->id}}', 'Activate E-Mail')"><i class="fa fa-times-circle text-danger"></i></a>
@endif
@endif

@if(isset($courses))
<a onclick="getUserCourses('{{route('users.courses', ['user' => $item->id])}}', '{{$item->full_name}}')" href="javascript:void(0)"><span class="label label-primary rounded-0 btn-link">{{ $item->courses_count }}</span></a>
@endif

@if(isset($actions))
{!! Form::open(['route' => ['users.activate_email', ['user' => $item->id]],'id' => 'activate_email-'.$item->id]) !!}
@method("PUT")
{!! Form::close() !!}

{!! Form::open(['route' => ['users.lock', ['user' => $item->id]],'id' => 'lock-'.$item->id]) !!}
@method("post")
{!! Form::close() !!}

{!! Form::open(['route' => ['users.unlock', ['user' => $item->id]],'id' => 'unlock-'.$item->id]) !!}
@method("post")
{!! Form::close() !!}

@if($item->status)
    <a href="javascript:void(0);"
       onclick="confirmActionWithAlert('{{'#lock-'.$item->id}}', 'Block')"
       class='btn btn-icon btn-outline-danger btn-circle btn-md'
       data-toggle="tooltip" title="حظر الطالب"><i class="fa fa-lock"></i>
    </a>
@else
    <a href="javascript:void(0);"
       onclick="confirmActionWithAlert('{{'#unlock-'.$item->id}}', 'Activate')"
       class='btn btn-icon btn-outline-success btn-circle btn-md'
       data-toggle="tooltip" title="تفعيل الطالب"><i class="fa fa-unlock"></i>
    </a>
@endif
@endif
