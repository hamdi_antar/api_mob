<div class="card-body">
    <div class="row">

        <div class="form-group col-md-6">
            <label>  الاسم الأول <span class="text-danger">*</span></label>
            <input type="text" class="form-control" placeholder="الاسم الاول " name="first_name" value="{{isset($item) ? $item->first_name : old('first_name')}}">
        </div>

        <div class="form-group col-md-6">
            <label>  الاسم الثاني <span class="text-danger">*</span></label>
            <input type="text" class="form-control" placeholder="الاسم الثانى " name="last_name" value="{{isset($item) ? $item->last_name : old('last_name')}}">
        </div>

        <div class="form-group col-md-6">
            <label>   البريد الإلكتروني <span class="text-danger">*</span></label>
            <input type="email" class="form-control" placeholder=" البريد الإلكتروني" name="email" value="{{isset($item) ? $item->email : old('email')}}">
        </div>

        <div class="form-group col-md-6">
            <label>    رقم الهاتف <span class="text-danger">*</span></label>
            <input type="text" class="form-control" placeholder="رقم الهاتف " name="phone" value="{{isset($item) ? $item->phone : old('phone')}}">
        </div>

        <div class="form-group col-md-6">
            <label>     كلمة المرور <span class="text-danger">*</span></label>
            <input type="password" autocomplete="new-password" class="form-control" placeholder="كلمة المرور" name="password" value="">
        </div>

        <div class="form-group col-md-6">
            <label> تأكيد كلمة المرور <span class="text-danger">*</span></label>
            <input type="password" autocomplete="new-password" class="form-control" placeholder="تأكيد كلمة المرور " name="password_confirmation" value="">
        </div>


        <div class="form-group col-md-6">
            <label> اختر الدور <span class="text-danger">*</span></label>
            <select class="form-control rounded-0 selectpicker" data-live-search="true" name="roles[]" multiple>
                <option value="">أختر الدور</option>
                @foreach($roles as $role)
                    <option value="{{$role->id}}"  {{isset($item) && in_array( $role->id, $item->roles->pluck('id')->toArray()) ? 'selected' : ''}}> {{ $role->name }}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group col-md-6">
            <input type="hidden" name="status" value="0">
            <label class="checkbox mt-5">
                <input type="checkbox" {{isset($item) && $item->status ? 'checked' : ''}} name="status" value="1">
                <span class="mr-4"></span>حالة التفعيل</label>
        </div>
    </div>
</div>
