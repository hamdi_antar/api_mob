@extends('layout.main')
@section('page', 'المستخدمين')
@php
    $page = 'users';
    $sub_page = 'users_with_roles';
@endphp
@section('sub-header')
    <div class="subheader py-2 py-lg-6 subheader-solid" id="kt_subheader">
        <div class="container-fluid">
            <div class="row mt-5">
                <div class="col-md-8">
                    <ol class="breadcrumb text-muted fs-6 fw-bold bg-white">
                        <li class="breadcrumb-item pe-3"><a href="#" class="pe-3 font-weight-boldest">المستخدمين</a>
                        </li>
                    </ol>
                </div>
                <div class="col-md-4 text-right">
                    <a href="{{route('users_with_roles.create')}}"
                       class="btn btn-sm btn-primary rounded-0 font-weight-bold command">
                        <i class="flaticon-plus"></i>
                        @lang('common.New')
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <div class="card card-custom">
        <div class="card-header flex-wrap pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">المستخدمين [<span
                            class="text-danger pt-2">{{ $items->count() }}</span>] </h3>
            </div>
            <div class="card-toolbar">
            </div>
        </div>
        <div class="card-body">
            <table id="datatable-with-btns" class="table table-bordered table-checkable  text-center">
                <thead>
                <tr>
                    <th class='text-center'>#</th>
                    <th class='text-center'>اسم المستخدم</th>
                    <th class='text-center'> البريد الإلكتروني</th>
                    <th class='text-center'>رقم الهاتف</th>
                    <th class='text-center'>الأدوار </th>
                    <th class='text-center'>تاريخ التسجيل </th>
                    <th class='text-center'>@lang('common.Action')</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
@endsection
@push('js')
    <script type="text/javascript">
        server_side_datatable('#datatable-with-btns');
    </script>
@endpush
