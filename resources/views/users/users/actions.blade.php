

@if(isset($roles))
<a href="javascript:void(0)"><span class="label label-primary rounded-0 btn-link">{{ count($item->roles) }}</span></a>
@endif

@if(isset($actions))


    {!! Form::open(['route' => ['users_with_roles.destroy', $item->id],'id' => 'delete-'.$item->id]) !!}
    @method("delete")
    {!! Form::close() !!}

    <a href="javascript:void(0);"
       onclick="confirmActionWithAlert('{{'#delete-'.$item->id}}', 'Delete')"
       class='btn btn-icon btn-outline-danger btn-circle btn-sm'
       data-toggle="tooltip" title="@lang('common.Delete')"><i class="flaticon2-trash fa-1x"></i>
    </a>
    <a href="{{route('users_with_roles.edit', $item->id)}}"
       class='btn btn-icon btn-outline-primary btn-circle btn-sm'
       data-toggle="tooltip" title="@lang('common.Edit')"><i class="flaticon-edit fa-1x"></i>
    </a>

    <a href="{{route('users_with_roles.log', $item->id)}}"
       class='btn btn-icon btn-outline-primary btn-circle btn-sm'
       data-toggle="tooltip" title="سجلات النشاط"><i class="flaticon-statistics fa-1x"></i>
    </a>

{!! Form::open(['route' => ['users.lock', ['user' => $item->id, 'roles' => 1]],'id' => 'lock-'.$item->id]) !!}
@method("post")
{!! Form::close() !!}

{!! Form::open(['route' => ['users.unlock', ['user' => $item->id, 'roles' => 1]],'id' => 'unlock-'.$item->id]) !!}
@method("post")
{!! Form::close() !!}

@if($item->status)
    <a href="javascript:void(0);"
       onclick="confirmActionWithAlert('{{'#lock-'.$item->id}}', 'Block')"
       class='btn btn-icon btn-outline-danger btn-circle btn-sm'
       data-toggle="tooltip" title="حظر "><i class="fa fa-lock fa-1x"></i>
    </a>
@else
    <a href="javascript:void(0);"
       onclick="confirmActionWithAlert('{{'#unlock-'.$item->id}}', 'Activate')"
       class='btn btn-icon btn-outline-success btn-circle btn-sm'
       data-toggle="tooltip" title="تفعيل "><i class="fa fa-unlock fa-1x"></i>
    </a>
@endif
@endif
