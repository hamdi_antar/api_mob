<table class="table">
    <thead>
    <tr>
        <th scope="col">#</th>
        <th scope="col">اسم الكورس</th>
        <th scope="col">اسم المعلم</th>
        <th scope="col">الباقة المشترك بها</th>
        <th scope="col">طريقة الدفع</th>
        <th scope="col">حالة الدفع</th>
        <th scope="col">تاريخ الاشتراك</th>
    </tr>
    </thead>
    <tbody>
    @if(count($user->userCourses) > 0)
        @foreach($user->userCourses as $index => $userCourse)
            <tr>
                <td>{{$index + 1}}</td>
                <td>{{optional($userCourse->course)->course_name}}</td>
                <td>{{optional($userCourse->course)->teacher_name}}</td>
                <td>{{optional($userCourse->package)->name}}</td>
                <td>{{ $userCourse->paymentMethod }}</td>
                <td>
                    @if($userCourse->orderStatus == 'PAID')
                        <span class="label label-inline label-light-success font-weight-bold">{{$userCourse->orderStatus}}</span>
                    @else
                        <span class="label label-inline label-light-danger font-weight-bold">{{$userCourse->orderStatus}}</span>
                    @endif
                </td>
                <td>{{ $userCourse->created_at }}</td>
            </tr>
        @endforeach
    @else
        <tr>
            <td colspan="7" class="text-center text-muted mt-5">لا يوجد كورسات مشترك بها</td>
        </tr>
    @endif
    </tbody>
</table>
