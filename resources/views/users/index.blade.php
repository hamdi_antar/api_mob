@extends('layout.main')
@section('page', 'تصنيفات الكورسات')
@php
    $page = 'users';
    $sub_page = 'students';
@endphp
@section('sub-header')
    <div class="subheader py-2 py-lg-6 subheader-solid" id="kt_subheader">
        <div class="container-fluid">
            <div class="row mt-5">
                <div class="col-md-8">
                    <ol class="breadcrumb text-muted fs-6 fw-bold bg-white">
                        <li class="breadcrumb-item pe-3"><a href="#" class="pe-3 font-weight-boldest">المستخدمين</a>
                        </li>
                        <li class="breadcrumb-item px-3 text-muted font-weight-boldest">المشتركــيين</li>
                    </ol>
                </div>
                <div class="col-md-4 text-right">
                    <a href="{{route('users.activate_emails_for_all_users')}}" class="btn btn-secondary" style="font-size: 10px"><i class="flaticon-mail"></i> تفعيل البريد الالكترونى لكل المشتركيين</a>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <div class="card card-custom">
        <div class="card-header flex-wrap pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">المشتركــيين فى المنصة [<span
                            class="text-danger pt-2">{{ $items->count() }}</span>] </h3>
            </div>
            <div class="card-toolbar">
            </div>
        </div>
        <div class="card-body">
            <table id="datatable-with-btns" class="table table-bordered table-checkable  text-center">
                <thead>
                <tr>
                    <th class='text-center'>#</th>
                    <th class='text-center'>اسم المشترك</th>
                    <th class='text-center'> البريد الألكترونى</th>
                    <th class='text-center'>رقم الهاتف</th>
                    <th class='text-center'>الكورسات </th>
                    <th class='text-center'>تفعيل البريد </th>
                    <th class='text-center'>تاريخ التسجيل </th>
                    <th class='text-center'>@lang('common.Action')</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>


    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body  d-none" id="responseData">
                </div>

                <div class="modal-body m-auto" id="progressBeforeRequest">
                   <div class="row text-center">
                       <div class="col-md-12">
                           <div class="spinner spinner-primary spinner-lg mr-15"></div>
                       </div>
                   </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-light-danger font-weight-bold" data-dismiss="modal">
                        <i class="fa fa-times"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
<script type="text/javascript">
    server_side_datatable('#datatable-with-btns');
    function getUserCourses(url, fullName) {
        $("#exampleModalLabel").html(fullName)
        $('#exampleModal').modal('show');
        $.ajax({
            url: url,
            type: "get",
            headers: {
                "X-CSRF-TOKEN": '{{csrf_token()}}'
            },
            success: function (data) {
                $("#progressBeforeRequest").addClass('d-none');
                $("#responseData").removeClass('d-none');
                $("#responseData").html(data.view);
            },
            error: function (jqXhr, json, errorThrown) {},
        });
    }
    $("#exampleModal").on("hidden.bs.modal", function () {
        $("#responseData").html('');
        $("#responseData").addClass('d-none');
        $("#progressBeforeRequest").removeClass('d-none');
    });
</script>
@endpush
