<li class="menu-item menu-item-submenu {{ isPageActive ('roles_permissions', $page??null)? 'menu-item-open menu-item-here': null}}" aria-haspopup="true" data-menu-toggle="hover">
    <a href="javascript:void(0);" class="menu-link menu-toggle">
        <span class="svg-icon menu-icon ">
            <i class="fas fa-users {{ isPageActive ('roles_permissions', $page??null)? 'text-primary': null}}"></i>
        </span>
        <span class="menu-text">الأدوار والصلاحيات</span>
        <i class="menu-arrow"></i>
    </a>
    <div class="menu-submenu">
        <i class="menu-arrow"></i>
        <ul class="menu-subnav">
            <li class="menu-item {{ isPageActive ('permissions', $sub_page??null)?'menu-item-active':null}}" aria-haspopup="true" data-menu-toggle="hover">
                <a href="{{route('permissions.index')}}" class="menu-link menu-toggle">
                    <i class="menu-bullet menu-bullet-dot">
                        <span></span>
                    </i>
                    <span class="menu-text">الصلاحيات</span>
                </a>
            </li>

            <li class="menu-item {{ isPageActive ('roles', $sub_page??null)?'menu-item-active':null}}" aria-haspopup="true" data-menu-toggle="hover">
                <a href="{{route('roles.index')}}" class="menu-link menu-toggle">
                    <i class="menu-bullet menu-bullet-dot">
                        <span></span>
                    </i>
                    <span class="menu-text">الأدوار</span>
                </a>
            </li>
        </ul>
    </div>
</li>


<li class="menu-item menu-item-submenu {{ isPageActive ('users', $page??null)? 'menu-item-open menu-item-here': null}}" aria-haspopup="true" data-menu-toggle="hover">
    <a href="javascript:;" class="menu-link menu-toggle">
        <span class="svg-icon menu-icon ">
            <i class="fas fa-users {{ isPageActive ('users', $page??null)? 'text-primary': null}}"></i>
        </span>
        <span class="menu-text">@lang('pages.Users')</span>
        <i class="menu-arrow"></i>
    </a>
    <div class="menu-submenu">
        <i class="menu-arrow"></i>
        <ul class="menu-subnav">
            <li class="menu-item menu-item-parent {{ isPageActive ('users', $page??null)??null}}" aria-haspopup="true">
                <span class="menu-link ">
                    <span class="menu-text">@lang('pages.Users')</span>
                </span>
            </li>
            <li class="menu-item {{ isPageActive ('students', $sub_page??null)?'menu-item-active':null}}" aria-haspopup="true" data-menu-toggle="hover">
                <a href="{{route('users.index')}}" class="menu-link menu-toggle">
                    <i class="menu-bullet menu-bullet-dot">
                        <span></span>
                    </i>
                    <span class="menu-text">المشتركــيين</span>
                </a>
            </li>

            <li class="menu-item {{ isPageActive ('users_with_roles', $sub_page??null)?'menu-item-active':null}}" aria-haspopup="true" data-menu-toggle="hover">
                <a href="{{route('users_with_roles.index')}}" class="menu-link menu-toggle">
                    <i class="menu-bullet menu-bullet-dot">
                        <span></span>
                    </i>
                    <span class="menu-text">المستخدمين</span>
                </a>
            </li>
        </ul>
    </div>
</li>


<li class="menu-item menu-item-submenu {{ isPageActive ('courses_and_categories', $page??null)? 'menu-item-open menu-item-here': null}}" aria-haspopup="true" data-menu-toggle="hover">
    <a href="javascript:;" class="menu-link menu-toggle">
        <span class="svg-icon menu-icon ">
            <i class="fas fa-file-code {{ isPageActive ('courses_and_categories', $page??null)? 'text-primary': null}}"></i>
        </span>
        <span class="menu-text">الكورســـات</span>
        <i class="menu-arrow"></i>
    </a>
    <div class="menu-submenu">
        <i class="menu-arrow"></i>
        <ul class="menu-subnav">
            <li class="menu-item {{ isPageActive ('categories', $sub_page??null)?'menu-item-active':null}}" aria-haspopup="true" data-menu-toggle="hover">
                <a href="{{route('categories.index')}}" class="menu-link menu-toggle">
                    <i class="menu-bullet menu-bullet-dot">
                        <span></span>
                    </i>
                    <span class="menu-text">تصنيفــات الكورسات</span>
                </a>
            </li>

            <li class="menu-item {{ isPageActive ('courses', $sub_page??null)?'menu-item-active':null}}" aria-haspopup="true" data-menu-toggle="hover">
                <a href="{{route('courses.index')}}" class="menu-link menu-toggle">
                    <i class="menu-bullet menu-bullet-dot">
                        <span></span>
                    </i>
                    <span class="menu-text">الكورسات</span>
                </a>
            </li>

            <li class="menu-item {{ isPageActive ('packages', $sub_page??null)?'menu-item-active':null}}" aria-haspopup="true" data-menu-toggle="hover">
                <a href="{{route('packages.index')}}" class="menu-link menu-toggle">
                    <i class="menu-bullet menu-bullet-dot">
                        <span></span>
                    </i>
                    <span class="menu-text">نظام الباقات</span>
                </a>
            </li>
        </ul>
    </div>
</li>



<li class="menu-item {{ isPageActive ('free_subscribers', $page??null)??null}}" aria-haspopup="true">
    <a href="{{ route('free_subscribe.index') }}" class="menu-link">
                        <span class="svg-icon menu-icon">
                            <i class="fa fa-comment-dots"></i>
                        </span>
        <span class="menu-text"> الاشتراكات اليدوية</span>
    </a>
</li>

<li class="menu-item {{ isPageActive ('settings', $page??null)??null}}" aria-haspopup="true">
    <a href="{{ route('settings.create') }}" class="menu-link">
                        <span class="svg-icon menu-icon">
                            <i class="fa fa-cogs"></i>
                        </span>
        <span class="menu-text"> الإعدادت العامة </span>
    </a>
</li>


<li class="menu-item {{ isPageActive ('social_media', $page??null)??null}}" aria-haspopup="true">
    <a href="{{ route('social_media.create') }}" class="menu-link">
                        <span class="svg-icon menu-icon">
                            <i class="fa fa-hashtag"></i>
                        </span>
        <span class="menu-text">  السوشيل ميديا </span>
    </a>
</li>



