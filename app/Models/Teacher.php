<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Hash;

class Teacher extends Model
{
    protected $table = 'teachers';

    public $timestamps = false;

    protected $fillable = [
        'name',
        'age',
        'gender',
        'email',
        'password',
        'phone',
        'user_id',
        'salary',
    ];

    protected $hidden = ['user_id', 'password'];

    public function setPasswordAttribute($password)
    {
        if (!is_null($password)) {
            $this->attributes['password'] = Hash::make($password);
        }
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
