<?php

namespace App\Http\Controllers\API;

use App\Models\Admin;
use App\Models\Student;
use App\Models\Teacher;
use JWTAuth;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\API\LoginRequest;
use App\Http\Requests\API\RegisterRequest;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpFoundation\Response;

class AuthController extends ApiController
{

    public function login(LoginRequest $request): JsonResponse
    {
        $credentials = $request->only('email', 'password');
        if ($token = JWTAuth::attempt($credentials)) {
            $user = User::whereEmail($request->email)->first();
            if ($user && $user->status === 0) {
                return response()->json(['error' => 'تم حظر الاكونت من خلال مدير المنصة..برجاء التواصل مع ادارة المنصة'], 401);
            }
            return $this->respondWithToken($user, $token);
        }
        return response()->json(['error' => 'البريد الإلكتروني أو كلمه المرور غير صحيحة'], 401);
    }

    public function me(): JsonResponse
    {
        return response()->json($this->guard()->user());
    }

    public function logout(): JsonResponse
    {
        $this->guard()->logout();
        return response()->json(['message' => 'Successfully logged out']);
    }

    public function refresh(): JsonResponse
    {
        return $this->respondWithToken($this->guard()->refresh());
    }

    protected function respondWithToken(User $user, string $token): JsonResponse
    {
        $info = null;
        if ($user->type == 'admin') {
            $info = Admin::where('user_id', $user->id)->first();
        }
        if ($user->type == 'student') {
            $info = Student::where('user_id', $user->id)->first();
        }
        if ($user->type == 'teacher') {
            $info = Teacher::where('user_id', $user->id)->first();
        }
        return response()->json([
            'user_data' => $info,
            'role' => $user->type,
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => $this->guard()->factory()->getTTL() * 60
        ]);
    }

    public function guard(): Guard
    {
        return Auth::guard('api');
    }
}
