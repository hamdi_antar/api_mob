<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\RegisterRequest;
use App\Http\Requests\API\RegisterStudentRequest;
use App\Http\Requests\API\UpdateStudentRequestStudent;
use App\Models\Admin;
use App\Models\Student;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use JWTAuth;

class StudentController extends ApiController
{

    public function index(): JsonResponse
    {
        $items = Student::all();
        return $this->apiResponse->setData($items)->setCode(200)->create();
    }

    public function update(UpdateStudentRequestStudent $request, Student $student): JsonResponse
    {
        $user = $student->user;
        $userData = [];
        $userData['email'] = $request->email;
        $userData['password'] = $request->password;
        $user->update($userData);
        $student->update($request->all());
        return $this->apiResponse->setMessages(['student updated successfully'])->setCode(200)->create();
    }

    public function destroy(Student $student): JsonResponse
    {
        $student->delete();
        $student->user()->delete();
        return $this->apiResponse->setMessages(['student deleted successfully'])->setCode(200)->create();
    }

    public function register(RegisterStudentRequest $request): JsonResponse
    {
        $data = $request->validated();
        $data['type'] = 'student';
        $user = User::create($data);
        $data['user_id'] = $user->id;
        $admin = Student::create($data);
        $token = JWTAuth::fromUser($user);
        return $this->apiResponse->setMessages(['student registered successfully'])->setCode(200)->create();
    }
}
