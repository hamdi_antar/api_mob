<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\RegisterStudentRequest;
use App\Http\Requests\API\UpdateStudentRequestStudent;
use App\Models\Student;
use App\Models\Teacher;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use JWTAuth;

class TeacherController extends ApiController
{
    public function index(): JsonResponse
    {
        $items = Teacher::all();
        return $this->apiResponse->setData($items)->setCode(200)->create();
    }

    public function update(UpdateStudentRequestStudent $request, Teacher $teacher): JsonResponse
    {
        $user = $teacher->user;
        $userData = [];
        $userData['email'] = $request->email;
        $userData['password'] = $request->password;
        $user->update($userData);
        $teacher->update($request->all());
        return $this->apiResponse->setMessages(['teacher updated successfully'])->setCode(200)->create();
    }

    public function destroy(int $teacher): JsonResponse
    {
       $teacher = Teacher::findOrFail($teacher);
        $teacher->delete();
        $teacher->user()->delete();
        return $this->apiResponse->setMessages(['teacher deleted successfully'])->setCode(200)->create();
    }

    public function register(RegisterStudentRequest $request): JsonResponse
    {
        $data = $request->validated();
        $data['type'] = 'teacher';
        $user = User::create($data);
        $data['user_id'] = $user->id;
        $teacher = Teacher::create($data);
        $token = JWTAuth::fromUser($user);
        return $this->apiResponse->setMessages(['teacher registered successfully'])->setCode(200)->create();
    }
}
