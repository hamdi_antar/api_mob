<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Responses\ApiResponse;
use App\Traits\LoggerError;

class ApiController extends Controller
{
    use LoggerError;

    /**
     * @var ApiResponse
     */
    protected $apiResponse;

    /**
     * @param ApiResponse $apiResponse
     */
    public function __construct(ApiResponse $apiResponse)
    {
        $this->apiResponse = $apiResponse;
    }
}
