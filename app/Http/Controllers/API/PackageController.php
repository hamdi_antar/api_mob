<?php

namespace App\Http\Controllers\API;

use Exception;
use App\Models\Course;
use App\Models\Package;
use App\Models\UserCourse;
use App\Traits\LoggerError;
use Illuminate\Http\Request;
use App\Responses\ApiResponse;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use App\Http\Requests\API\PackageSubscribeRequest;

class PackageController extends ApiController
{


    public function getPackages(): JsonResponse
    {
        $items = Package::active()->get();
        return $this->apiResponse->setCode(200)->setData($items)->create();
    }

    public function subscribeWithPackage(PackageSubscribeRequest $request): JsonResponse
    {
        try {
            $courses = Course::all();
            foreach ($courses as $course) {
                $userCourseExist = UserCourse::where('course_id', $course->id)->where('user_id', $request->user_id)->first();
                if (!$userCourseExist) {
                    UserCourse::create($request->merge(['course_id' => $course->id])->all());
                }
            }
            return $this->apiResponse->setCode(200)->setData([])->setMessages(['user has been subscribed in all courses successfully'])->create();
        } catch (Exception $exception) {
            $this->logErrors($exception);
            return $this->apiResponse->setCode(422)->setData([])->setErrors(['something went wrong'])->create();
        }
    }
}
