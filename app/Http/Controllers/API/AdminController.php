<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\LoginRequest;
use App\Http\Requests\API\RegisterRequest;
use App\Models\Admin;
use App\Models\User;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use JWTAuth;

class AdminController extends ApiController
{
    public function register(RegisterRequest $request): JsonResponse
    {
        $data = $request->validated();
        $data['type'] = 'admin';
        $user = User::create($data);
        $data['user_id'] = $user->id;
        $admin = Admin::create($data);
        $token = JWTAuth::fromUser($user);
        return $this->apiResponse->setMessages(['admin registered successfully'])->setCode(200)->create();
    }
}
