<?php

namespace App\Http\Requests\API;

class UpdateUserRequest extends BaseApiRequest
{
    public function rules(): array
    {
        $userId = request()->segment(3);
        return [
            'last_name' => 'required|string|max:200',
            'first_name' => 'required|string|max:200',
            'phone' => 'max:20',
            'email' => 'required|email|max:200|unique:users,email,'.$userId,
            'image' => 'nullable|image',
        ];
    }
}
