<?php

namespace App\Http\Requests\API;

class SubscribeRequest extends BaseApiRequest
{
    public function rules(): array
    {
        return [
            'user_id' => 'required|exists:users,id',
            'course_id' => 'required|exists:courses,id',
            'type' => 'required',
            'referenceNumber' => 'required',
            'merchantRefNumber' => 'required',
            'orderAmount' => 'required',
            'paymentAmount' => 'required',
            'fawryFees' => 'required',
            'paymentMethod' => 'required',
            'orderStatus' => 'required',
        ];
    }
}
