<?php

namespace App\Http\Requests\API;

class ContactUsRequest extends BaseApiRequest
{
    public function rules(): array
    {
        return [
            'email' => 'required|email',
            'name' => 'required|max:200',
            'phone' => 'required|max:15',
            'message' => 'required|min:2',
        ];
    }
}
