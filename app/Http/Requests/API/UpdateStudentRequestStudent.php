<?php

namespace App\Http\Requests\API;

class UpdateStudentRequestStudent extends BaseApiRequest
{
    public function rules(): array
    {
        return [
            'name' => 'required|min:1|max:100',
            'email' => 'required|email|unique:users,id,'.$this->segment(3),
            'password' => 'required|confirmed|min:6',
            'age' => 'nullable|numeric',
            'gender' => 'required|in:male,female',
            'phone' => 'nullable',
            'address' => 'nullable',
            'salary' => 'nullable',
        ];
    }
}
