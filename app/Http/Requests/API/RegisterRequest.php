<?php

namespace App\Http\Requests\API;

class RegisterRequest extends BaseApiRequest
{
    public function rules(): array
    {
        return [
            'name' => 'required|min:1|max:100',
            'email' => 'required|email|unique:users|max:200',
            'password' => 'required|confirmed|min:6',
        ];
    }
}
