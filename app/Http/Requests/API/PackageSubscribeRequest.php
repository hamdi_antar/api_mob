<?php

namespace App\Http\Requests\API;

class PackageSubscribeRequest extends BaseApiRequest
{
    public function rules(): array
    {
        return [
            'user_id' => 'required|exists:users,id',
            'package_id' => 'required|exists:packages,id',
            'type' => 'required',
            'referenceNumber' => 'required',
            'merchantRefNumber' => 'required',
            'orderAmount' => 'required',
            'paymentAmount' => 'required',
            'fawryFees' => 'required',
            'paymentMethod' => 'required',
            'orderStatus' => 'required',
        ];
    }
}
