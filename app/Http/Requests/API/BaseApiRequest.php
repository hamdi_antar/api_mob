<?php

namespace App\Http\Requests\API;

use App\Responses\ApiResponse;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Symfony\Component\HttpFoundation\Response;

class BaseApiRequest extends FormRequest
{
    protected function failedValidation(Validator $validator)
    {
        $apiResponse = app(ApiResponse::class);
        $response = $apiResponse
            ->setCode(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->setErrors($validator->errors()->toArray())
            ->create();
        throw new HttpResponseException($response);
    }
}
