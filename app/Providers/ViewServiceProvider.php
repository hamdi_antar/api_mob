<?php

namespace App\Providers;

use App\Models\City;
use App\Models\ClassCategory;
use App\Models\ClassModel;
use App\Models\Country;
use App\Models\Lecture;
use App\Models\Level;
use App\Models\School;
use App\Models\Stage;
use App\Models\Subject;
use App\Models\SubjectCategory;
use App\Models\TimePeriod;
use App\Models\User;
use App\Models\UserParent;
use App\Models\UserType;
use App\Models\Year;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
    }
}
