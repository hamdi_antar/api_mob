<?php

use Illuminate\Support\Facades\Route;

Route::post('login', 'AuthController@login');
Route::post('admin/register', 'AdminController@register');
Route::post('student/register', 'StudentController@register');
Route::post('teacher/register', 'TeacherController@register');

Route::group(['middleware' => ['jwt.verify']], function () {
    //admin routes
    Route::post('admin/register', 'AdminController@register');

    //student routes
    Route::get('student/all', 'StudentController@index');
    Route::put('student/{student}', 'StudentController@update');
    Route::delete('student/{student}', 'StudentController@destroy');

    //teacher routes
    Route::get('teacher/all', 'TeacherController@index');
    Route::put('teacher/{teacher}', 'TeacherController@update');
    Route::delete('teacher/{student}', 'TeacherController@destroy');

});
