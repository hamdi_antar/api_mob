<?php

namespace Database\Seeders;

use App\Models\Admin;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Seeder;
use DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'email' => 'admin@admin.com',
            'password' => 123456,
            'type' => 'admin',
        ]);

        $admin = Admin::create([
            'name' => 'admin',
            'email' => 'admin@admin.com',
            'password' => 123456,
            'user_id' => $user->id,
        ]);
    }
}
